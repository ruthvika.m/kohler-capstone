import pandas as pd
import constant as c



#day_level=pd.read_excel(c.day_level_location,engine = c.engine)
day_level=pd.read_csv(c.day_level_location_csv)



required_columns = [c.calendar_year_month,c.material,c.brand,c.quantity,c.profit,
                                c.item_value_cost,c.item_value_gross,c.item_value_gross,c.item_value_net]

checker = {}
calendar_datatype_error = -1
null_threshold_flag = -1
required_column_check_flag = -1
quantity_datatype_error = -1
profit_datatype_error = -1
item_value_list_datatype_error = -1
item_value_cost_datatype_error = -1
item_value_net_datatype_error = -1
item_value_gross_datatype_error = -1

        
def calendar_year_month_check():
    day_level[c.calendar_year_month] = day_level[c.calendar_year_month].fillna(0)

    try:
        day_level[c.calendar_year_month].astype(int)
        calendar_datatype_error = 0


    except:
        calendar_datatype_error = 1
    return calendar_datatype_error

def null_check():
    ''' checks for null percentage of required columns and updates threshold flag'''
    mpnull = day_level[required_columns].isnull().sum()/len(day_level[required_columns])*100
    totalmiss = mpnull.sum().round(2)
    if totalmiss <= 30:
        null_threshold_flag = 0    
    else:
        null_threshold_flag = 1
    return null_threshold_flag

def column_checks():
    '''checks for existence of required columns '''
    if set(required_columns).issubset(day_level.columns):
        required_column_check_flag = 0
    else:
        required_column_check_flag = 1

    return required_column_check_flag


def quantitative_datatype_error():
    try:
        day_level[c.quantity]=(day_level[c.quantity]).astype(int)
        quantity_datatype_error = 0
    except:
        quantity_datatype_error = 1

    try:
        day_level[c.profit]=(day_level[c.profit]).astype(float)
        profit_datatype_error = 0
    except:
        profit_datatype_error = 1


    try:
        day_level[c.item_value_net]=(day_level[c.item_value_net]).astype(float)
        item_value_net_datatype_error = 0
    except:
        item_value_net_datatype_error = 1

    try:
        day_level[c.item_value_list]=(day_level[c.item_value_list]).astype(float)
        item_value_list_datatype_error = 0
    except:
        item_value_list_datatype_error = 1

    try:
        day_level[c.item_value_gross]=(day_level[c.item_value_gross]).astype(float)
        
        item_value_gross_datatype_error = 0
    except:
        item_value_gross_datatype_error = 1
    try:
        day_level[c.item_value_cost]=(day_level[c.item_value_cost]).astype(float)
        item_value_cost_datatype_error = 0
    except:
        item_value_cost_datatype_error = 1

    return [item_value_cost_datatype_error, item_value_list_datatype_error, item_value_gross_datatype_error, 
            item_value_net_datatype_error, quantity_datatype_error, profit_datatype_error]

    
def run_checks():
    ''' runs all checks and returns a dictionary with checker name and flag 
        ex: null_threshold_check : 1 if file does not pass null threshold check '''
    checker[c.dl_err1] = calendar_year_month_check()
    checker[c.dl_err2] = null_check()
    checker[c.dl_err3] = column_checks()

    datatype_list = quantitative_datatype_error()

    for i,j in zip(c.err_list,datatype_list):
        checker[i] = j


    print(checker)
    return checker

    #for i,err in enumerate(datatype_list):
        #err_code = "dl_err"
        #checker
run_checks()








