## general 
engine = "openpyxl"

##constants for material product 
material_product_location ="..\\orignal_files\\TMC_MATERIAL_PRODUCT.xlsx"
material = 'MATERIAL'
product_code = 'LEVEL1_PROD_CODE'
brand = 'BRAND'
mp_required_columns = [material,product_code,brand]

brand_unique_list=[1,2,3]
checker = {}

mp_error1 = 'Brand Category Error' 
mp_error2 ='Product Code Datatype Error' 
mp_error3 ='Material Format Error' 
mp_error4 ='Null Percentage Error' 
mp_error5 ='Required columns check' 


## material description files 

description = 'MATERIAL_DESCRIPTION'
material_description_location = "..\\orignal_files\\MATERIAL_DESCRIPTION.xlsx"

md_err1 = 'Material Format Error'
md_err2 = 'Null Percentage Error'
md_err3 = 'Required columns check'


## day level 

calendar_year_month = 'CALENDAR_YEAR_MONTH'

quantity = 'QTY_ORDER_CHANGE'
profit = 'PROFIT'
item_value_net = 'USD_ITEM_VALUE_NET'
item_value_list = 'USD_ITEM_VALUE_LIST'
item_value_cost = 'USD_ITEM_VALUE_COST'
item_value_gross = 'USD_ITEM_VALUE_GROSS'

day_level_location = "..\\orignal_files\\TMC_ALL_SKU_ADS_2016_DAY_LEVEL.xlsx"
day_level_location_csv = "..\\orignal_files\\csvs\\TMC_ALL_SKU_ADS_2016_DAY_LEVEL.csv"

dl_err1 = 'Calendar year month check'
dl_err2 = 'Null check'
dl_err3 = 'Required columns check'

err_list = ['item_value_cost_datatype_error', 'item_value_list_datatype_error', 'item_value_gross_datatype_error', 
            'item_value_net_datatype_error', 'quantity_datatype_error, profit_datatype_error']

## product categories 

product_categories_location = "..\\orignal_files\\PRODUCT_CATEGORIES.xlsx"
pc_brand = 'Brand'
brand_name = 'Brand_Name'
level1_prod_code = 'PH1'
market_category = 'Market_Category'
pc_required_columns = [pc_brand,brand_name,level1_prod_code,market_category]

brand_unique_list=[1,2,3]

pc_err1 = 'Brand Category Error'
pc_err2 = 'Level1 Product Code Error'
pc_err3 = 'Brand Name Starting character Error'
pc_err4 = 'Null Percentage Error'
pc_err5 = 'Required columns check'



### message lines 

line1 = ["\n\n Material Product\n\n"]
line2 = "\n\n Material Description\n\n"
line3 = "\n\n Product Categories \n\n"
line4 = "\n\n Day Level \n\n"
breaker = '---'
ending = '-------------'