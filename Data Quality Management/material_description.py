import pandas as pd
import constant as c


material_description=pd.read_excel(c.material_description_location,engine = c.engine)


required_columns = [c.material,c.description]
cannot_be_null = [c.material]
checker = {}

material_error = -1
null_threshold_flag = -1
required_column_check_flag = -1


quantity_datatype_error = -1
profit_datatype_error = -1
item_value_list_datatype_error = -1
item_value_cost_datatype_error = -1
item_value_net_datatype_error = -1
item_value_gross_datatype_error = -1

def material_check():
    '''
    checks for starting character of material being alpha numeric 
    '''

    for i in material_description[c.material]:
        if i[0].isalnum():
            material_error = 0
        else:
            material_error = 1

        return material_error


def column_checks():
    '''checks for existence of required columns '''
    if set(required_columns).issubset(material_description.columns):
        required_column_check_flag = 0
    else:
        required_column_check_flag = 1

    return required_column_check_flag

def null_check(required_column_check_flag):
    ''' checks for null percentage of required columns and updates threshold flag'''
    print(required_column_check_flag)
    mpnull = material_description[required_columns].isnull().sum()/len(material_description[required_columns])*100
    totalmiss = mpnull.sum().round(2)
    if totalmiss <= 30:
        null_threshold_flag = 0    
    else:
        null_threshold_flag = 1
    

    return null_threshold_flag

def run_checks():

    ''' runs all checks and returns a dictionary with checker name and flag 
        ex: null_threshold_check : 1 if file does not pass null threshold check '''
    
    checker[c.md_err1] = material_check()
    
    checker[c.md_err3] = column_checks()
    checker[c.md_err2] = null_check(required_column_check_flag)

    return checker



    
run_checks()   
print(checker)



