
material_description_location= "..\\orignal_files\\MATERIAL_DESCRIPTION.xlsx"
product_categories_location= "..\\orignal_files\\PRODUCT_CATEGORIES.xlsx"
day_level_location= "..\\orignal_files\\TMC_ALL_SKU_ADS_2016_DAY_LEVEL.xlsx"
material_product_location= "..\\orignal_files\\TMC_MATERIAL_PRODUCT.xlsx"

sum_col = ['TOTAL_QUANTITY','PROFIT','QTY_ORDER_CHANGE','USD_ITEM_VALUE_NET','USD_ITEM_VALUE_GROSS','USD_ITEM_VALUE_LIST','USD_ITEM_VALUE_COST','DISCOUNT']

agg_dict = {}
for i in sum_col:
    agg_dict[i] = 'sum'


quantitative_group_col = 'CALENDAR_YEAR_MONTH'

categorical_group_col = ['MATERIAL','CALENDAR_YEAR_MONTH']

date_col = 'DATE'

material_code = 'MATERIAL'

date_material = ['DATE','MATERIAL']

prod_code = "PROD_CODE"

lvl1_prod_code = 'LEVEL1_PROD_CODE'

month_level_sku_level_merged_on = ['PROD_CODE','BRAND']

merge_how_left = "left"

columns_to_be_dropped = ['Unnamed: 3', 'Unnamed: 4',
       'Unnamed: 5_x', 'Unnamed: 6_x', 'Unnamed: 7_x', 'Unnamed: 8','Unnamed: 5_y',
       'Unnamed: 6_y', 'Unnamed: 7_y']


